# Documentation

- [Presentation](#presentation)
- [Installation](#installation)
- [Usage](#usage)
	- [Command line arguments](#commandArguments)
	- [Basic configuration file](#configurationFile)
- [Simple example](#simpleExample)
- [Advanced configuration](#advancedConfiguration)
	- [Keys summary](#keysSummary)
	- [Global configuration](#globalConfiguration)
		- [Output and working directory](#outputDirectory)
		- [Column delimiter](#globalDelimiter)
	- [Input configuration](#inputConfiguration)
		- [Changing input files delimiter](#changeInputDelimiters)
		- [Changing root folder](#changeInputRootFolder)
		- [Enable / Disable the diff tracker](#diffTracker)
		- [Specifying the medias folder](#mediasFolder)
		- [Chaning rows before compilation](#beforeCompilationRowChange)
		- [Input files configuration](#inputFileConfiguration)
			- [File columns configuration](#fileColumnsConfiguration)
			- [Other options](#otherOptions)
		- [Indexes](#indexes)
			- [Variable indexes](#variableIndexes)
			- [Different indexes in different files](#differentIndexes)
			- [Soften matching index method](#indexAdapters)
	- [Output configuration](#outputConfiguration)
		- [Changing root folder](#changeOutputRootFolder)
		- [Changing the output delimiter](#changeOutputDelimiter)
		- [Changing the output inside delimiter](#changeOutputInsideDelimiter)
		- [Changing tracked differences between files naming conventions](#trackDiffTemplate)
		- [Changing or specifying the compiled php file name](#compiledPHP)
		- [Replacing one line by one or more](#replaceLines)
		- [Modifying/parsing the whole dataset before columns transformation](#modifyFinalDataset)
			- [Related products example](#relatedProductsExample)
		- [Column mapping](#columnMapping)
			- [Changing column name](#changeColumnName)
			- [Formatting or changing column value](#columnFormater)
			- [Creating a new column](#columnCreation)
			- [Excluding a column](#columnExclusion)
		- [Including unmapped columns in result](#includeUnmapped)
		- [Convert compiled values to string](#stringConverter)
		- [Medias mapping](#mediasMapping)
			- [Changing medias destination](#mediasDestination)
			- [Including medias destination to medias paths](#includeMediasPath)
			- [Making medias paths absolute](#absoluteMediasPaths)
			- [Changing medias key in compiled php format](#changingMediasKey)
			- [Excluding items without medias link](#excludingNoMediasItems)
			- [Enabling no medias report generation](#noMediasReport)
			- [Defining a no medias found fallback](#noMediasFallback)
			- [Filtering found medias paths](#mediasPathsFiltering)
			- [Renaming medias files](#mediasFileRenamer)
			- [Soften the medias mapping method](#mediaFileAdapter)
		- [Logs](#logsConfiguration)
			- [Enable file logging](#fileLogging)
			- [Changing logs template](#logsTemplate)
			- [Changing logs line appearance](#logsLineParams)

## <a name="presentation">Presentation</a>

`csvCompiler` is a standalone and single file csv compiler that allows you to aggregate one or more
csv files into one final csv file. It can handle data transformations and column transformations using
a config file written in PHP.

It was first created to handle data aggregation to generate an import.csv file for magento2.3 using several
manufacturer product Excel files, and a folder with all products pictures with SKU and/or EAN in the name.

The point of this program is :

- Defining index to delete duplicated rows
- Compile all data that can be found to build an exhaustive dataset
- Automaticaly associating each medias (here pictures) to a product using information contained in
each row (if needed).
- Generating an output file with all aggregated data AND produce reports about which data is missing
in some files (medias, incomplete rows, etc...)

If you clone this repo, you will end up with two files :

- `csvCompiler.php`, which is the compiler
- `doc.conf.php`, which is a documented file to explain succintly how you can configure the program to meet your needs.

**Word about performances :** Final performances strongly depends upon your configurations. With minimal config,
it should be really fast, but with many files and modifiers, you can end up with a few minutes of processing.

This compiler is not meant to be over performant, but to be adaptative, exhaustive and flexible.
However, to save time when trying to generate your final output, the compiler works in two steps:

1. It reads input files and generates a compiled php file.
2. It reads the compiled php file and generate the final output.

By default, the compiler will assume that the first file is the most complete one, and will try to
match the following against it. It means that if an item only appears in the other file, and not in the
first, it will be ignored.

That said, you can merge some files specifying their configuration at the beginning thanks to the
`add_if_not_found` attribute, as explained in [this section](#otherOptions).

## <a name="installation" >Installation</a>

Simply clone the repos and give execution permission to `csvCompiler` using the following commands :

```bash
git clone https://framagit.org/Ariart/csvcompiler.git
chmod +x ./csvcompiler/csvCompiler.php
```

You can install it globally into your system using this command :

```bash
sudo ln -s ~/csvcompiler/csvCompiler.php /usr/bin/csvCompiler
sudo chmod 777 /usr/bin/csvCompiler
```

## <a name="usage">Usage</a>

We will assume that we don't install the compiler globally, so we will start to move into the compiler's directory :

```bash
cd ~/csvcompiler
```

And then run the command `./csvCompiler.php -conf /absolute/path/to/conf.php` to start compilation.

### <a name="commandArguments">Command line arguments</a>

The compiler accepts the following arguments :

|   Argument   |Optional|          Parameters            |Effect|
|--------------|---------|--------------------------------|------|
|-conf         |   NO    |Absolute path to the config file|Specify the configuration file for compilation.|
|-clean        |   YES   |                                |Clean the `$config["dir"]` folder and restart the compilation. If not given will compile only if no compiled php file is found.|
|-quiet        |   YES   |                                |Disable all output logs into the console.|
|--compile-only|   YES   |                                |Disable the csv file generation and only do the compilation process.|

### <a name="configurationFile">Basic configuration file</a>

There is the minimum options a configuration fil must contain :

```php
<?php
$config = [
	//assuming that the working dir will be located into ~/result
	"dir" =>__DIR__."/../result",
	//default csv delimiter
	"delimiter" => ";",
	"input" => [
		//assuming that all input files will be located into ~/data
		"root" => __DIR__."/../data",
		//assuming that all medias files will be located into ~/medias
		//if not defined, the compiler will not try to link any medias to data.
		"medias" => __DIR__."/../medias",
		//will track all missing data in each file (if more than one)
		"track_diff" => true,
		//columns that can be used as index to retrieve row (can be an EAN, SKU or any unique data across lines)
		"indexes" => [
			"EAN" => [ "validator" => $eanValidator = "/^[\d]{13}$/" ]
		],
		//Define files that will be used for compilation
		"files" => [
			//the first file contains the product list
			"cmd" => [
				//assuming that products.csv is located into ~/data
				"path" => "products.csv",
				//define all csv columns (in order that the appears in products.csv from left to right).
				"columns" => [
					"EAN" => [ "not_empty" => true ],
					"SHORT_DESC" => [ "not_empty" => true ],
					"PRICE (€)" => [ "not_empty" => true ],
					"UNIT" => [ "not_empty" => true ],
					"QTT" => [ "default" => 0 ]
				]
			],
			//the second contains full description and categories
			"desc" => [
				//assuming that desc.csv in located in ~/data
				"path" => "desc.csv",
				//define all columns
				"columns" => [
					"EAN" => [ "not_empty" => true ],
					"TITLE" => [ "not_empty" => true ],
					"LONG_DESC" => [ "not_empty" => true ],
					"CATEG_1" => [ "not_empty" => true ],
					"CATEG_2" => [ "not_empty" => true ],
					"CATEG_3" => [ "not_empty" => true ]
				]
			]
		]
	],
	"output" => [
		//if relatives, the compiled file will be stored into $config["dir"] (~/result)
		"compiled" => "compiled.php",
		//if relatives, the compiled file will be stored into $config["dir"] (~/result)
		"generated_filename" => "compiled.csv",
		//this folder will get all matching image copy in there.
		"medias" => "pub/media/import",
		//if you want a report for lines with no medias found, define this option
		//if relatives, the compiled report will be stored into $config["dir"] (~/result)
		"no_medias" => "no_media_found.csv"
	]
];
```

For an exhaustive list and deeper explanations about each config keys, please see the
[advanced configuration section](#advanced configuration).

## <a name="simpleExample">Simple example</a>

We will assume that the config file is `~/config.php` and has the same content as the example above .

Just run the following command to parse all files accordingly to configurations :

```bash
./csvCompiler.php -conf ~/config.php
```

If `~/products.csv` contain the following data :

|EAN|SHORT_DESC|PRICE (€)|UNIT|QTT|
|---|----------|---------|----|---|
|1231235689458|A fancy product|100.32|boxe|30|

`~/cmd.csv` content is :

|EAN|TITLE|LONG_DESC|CATEG_1|CATEG_2|CATEG_3|
|---|-----|---------|-------|-------|-------|
|1231235689458|Fancy|A really fancy blue product.|Products|Fancy|Blue|

And the `~/medias` has the following structure :
```
.
..
72dpi
 |- 1231235689458 fancy blue.jpg
300dpi
 |- 1231235689458 fancy blue.jpg
```

The resulting file `~/result/compiled.csv` generated contains the following information :

|EAN|SHORT_DESC|PRICE (€)|UNIT|QTT|TITLE|LONG_DESC|CATEG_1|CATEG_2|CATEG_3|MEDIAS|
|---|----------|---------|----|---|-----|---------|-------|-------|-------|------|
|1231235689458|A fancy product|100.32|boxe|30|Fancy|A really fancy blue product.|Products|Fancy|Blue|/absolute/path/to/medias/72dpi/1231235689458 fancy blue.jpg,/absolute/path/to/medias/300dpi/1231235689458 fancy blue.jpg|

**Important note :** If you don't specify any `$config["output"]["columns_map"]`, the resulting file
will be the exact csv concatenation without duplication (based on indexes you've defined into `$config["input"]["indexes"]`).

## <a name="advancedConfiguration">Advanced configuration</a>

There are many options that you can set to configure the compiler's behavior, input files interpretation and output formats, so please
read the following section carefully to learn all that you can do with the compiler.

### <a name="keysSummary">Configuration keys summary</a>

Since there are many configuration keys, you will find below a full list of available config keys,
associated with the link to the according documentation section where you can find more information
about each of them :

**Important note :** if a config key can be arbitrarily chosen, it will be designed as `ANY`.

|                 Key                 |       Default         |                   Description/Section                  |
|-------------------------------------|-----------------------|--------------------------------------------------------|
|`$config['dir']`                     |   `/tmp/csvCompiler`  |          [Working directory](#outputDirectory)         |
|`$config['delimiter']`               |           `;`         |          [Default delimiter](#globalDelimiter)         |
|`$config['input']`                   |      **Required**     |        [Input configuration](#inputConfiguration)      |
|`$config['input']['delimiter']`      |`$config['delimiter']` |         [Input delimiter](#changeInputDelimiters)      |
|`$config['input']['root']`           |   `$config['dir']`    |        [Input root folder](#changeInputRootFolder)     |
|`$config['input']['medias']`         |         `null`        |            [Input medias folder](#mediasFolder)        |
|`$config['input']['indexes']`        |         `null`        |                 [Input indexes](#indexes)              |
|`$config['input']['indexes'][ANY]['validator']`|**Required** |           [Indexe validator](#indexes)                 |
|`$config['input']['indexes'][ANY]['adapters']`|**Required**  |           [Indexe adapters](#indexAdapters)            |
|`$config['input']['track_diff']`     |        `false`        |     [Enable/disable the diff tracker](#diffTracker)    |
|`$config['input']['row_modifier']`   |        `null`         |     [Modify a row before compilation](#beforeCompilationRowChange)    |
|`$config['input']['files'][ANY]`     |**At least one Required**| [Input files definitions](#inputFileConfiguration)   |
|`$config['input']['files'][ANY]['delimiter']`|`$config['input']['delimiter']`| [Input file delimiter](#otherOptions)  |
|`$config['input']['files'][ANY]['path']`|   **Required**     |   [Input file path](#inputFileConfiguration)           |
|`$config['input']['files'][ANY]['add_if_not_found']`|`false` |  [Add item if not previously found](#otherOptions)     |
|`$config['input']['files'][ANY]['row_filters']`|  `null`     |  [Row filters](#otherOptions)                          |
|`$config['input']['files'][ANY]['indexes']`|`$config['input']['indexes']`|  [File indexes](#otherOptions)             |
|`$config['input']['files'][ANY]['columns'][ANY]['default']`|`null`| [Default column value](#fileColumnsConfiguration) |
|`$config['input']['files'][ANY]['columns'][ANY]['not_empty']`|`false`| [Allow/disallow empty column value](#fileColumnsConfiguration) |
|`$config['input']['files'][ANY]['columns'][ANY]['modifier']`|`null`| [Modify/format a column value](#fileColumnsConfiguration) |
|`$config['output']`                  |      **Required**     |       [Output configuration](#outputConfiguration)     |
|`$config['output']['root']`          |    `$config['dir']`   |      [Output root folder](#changeOutputRootFolder)     |
|`$config['output']['delimiter']`     |           `;`         |      [Output delimiter](#changeOutputDelimiter)     |
|`$config['output']['inside_delimiter']`|         `,`         |      [Output inside delimiter](#changeOutputInsideDelimiter)     |
|`$config['output']['track_diff_template']`|`found_in_@from_but_not_in_@too.csv`|[Track diff report name template](#trackDiffTemplate)|
|`$config['output']['compiled']`      |     `compiled.php`    |[Compiled php file name and location](#compiledPHP)     |
|`$config['output']['line_replacer']` |         `null`        |[Split or filter a row](#replaceLines)                  |
|`$config['output']['dataset_modifier']`|         `null`      |[Modify the compiled dataset before transformation](#modifyFinalDataset)|
|`$config['output']['columns_map']`    |         `null`       |[Column map between php compiled file and result csv file](#columnMapping)|
|`$config['output']['columns_map'][ANY]['name']`|`null`       |[Column name](#changeColumnName)                        |
|`$config['output']['columns_map'][ANY]['exclude']`|`false`   |[Exclude the column from output](#excludeColumn)        |
|`$config['output']['columns_map'][ANY]['columnFormater']`|`null`|[Formatting or changing column value](#columnFormater)|
|`$config['output']['include_not_mapped_entries']`|`false`    |[Including unmapped column into the result](#includeUnmapped)|
|`$config['output']['string_converter']`|`null`               |[Convert compiled values to `string`](#stringConverter) |
|`$config['output']['medias']`        |         `medias`      |       [Medias folder destination](#mediasDestination)  |
|`$config['output']['include_medias_path']`|    `false`       |[Include `$config["output"]["medias"] in resulting medias path`](#includeMediasPath)  |
|`$config['output']['make_medias_paths_absolute']`|`false`    |       [Make medias paths absolute](#absoluteMediasPaths)  |
|`$config['output']['medias_key']`|`MEDIAS`                   |[Changing medias key into compiled php file](#changingMediasKey)|
|`$config['output']['remove_items_with_no_medias']`|`false`   |[Remove items that haven't been linked to at least one media](#excludingNoMediasItems)|
|`$config['output']['no_medias']`     |         `null`        |[Enable and specify "no medias" file generation report](#noMediasReport)|
|`$config['output']['medias_not_found_fallback']`| `null`     |[Fallback `callable` for rows without medias](#noMediasFallback)|
|`$config['output']['medias_filter']` |         `null`        |[Filtering medias paths](#mediasPathsFiltering)          |
|`$config['output']['medias_file_renamer']` |   `null`        |[Renaming medias files](#mediasFileRenamer)          |
|`$config['output']['medias_file_adapter']`|    `null`        |[Medias adapter](#mediaFileAdapter)              |
|`$config['output']['logs']['path']`  |         `null`        |[Enabling file logging](#logsConfiguration)                     |

### <a name="globalConfiguration">Global configuration</a>

The following keys are global and not declared inside the `$config['input']` scope nor in `$config['output']` scope,
and can be overridden in deeper config scopes.

#### <a name="outputDirectory">Output/working directory</a>

The `$config['dir']` key allows you to define the final directory for all output files. If not specified,
the default output directory will be `/tmp/csvCompiler`.

If a relative path is given, it will be resolved as a child of the `csvCompiler.php` directory (`__DIR__`).

**Note :** If the final directory and/or one or several of its parents doesn't exist,
the full path will be created.

#### <a name="globalDelimiter">Output directory</a>

The `$config['delimiter']` key allows you to change the default csv delimiter (`;`) by any `char` or `string`.

### <a name="inputConfiguration">Input configuration</a>

The key `$config["input"]` allows you to define and change the way the `csvCompiler` will handle your
input files, and how it will create the compiled php file.

#### <a name="changeInputDelimiters">Changing input files delimiter</a>

You can override the `$config['delimiter']` only for input configuration setting `$config['input']['delimiter']` key to a new
delimitation `char` or `string`.

**Note :** You can override it for each file specifying a `delimiter` key too. See more about this in the
[dedicated section about input files configuration](#inputFilesConfiguration).

#### <a name="changeInputRootFolder">Changing root folder</a>

In order to not forcing you to declare all paths in an absolute way, you can define a `$config["input"]["root"]` key
that will be used to resolve relative paths.

If you don't, relative paths will be resolved in the `csvCompiler.php` directory folder (`__DIR__`).

Whether you declare this key or not, you can always define absolute paths.

```php
$config = [
	//...
	"input" => [
		//...
		"root" => "/home/me"
	]
];
```

#### <a name="diffTracker">Enable / Disable the diff tracker</a>

The diff tracker creates a final report when compilation ends to track all lines in files that haven't
been found in the dataset, if the [`add_if_not_found` option](#otherOptions) is not set for the current file.

The final report will be a csv file using the source file delimiter `$config['input']['files'][ANY]['delimiter']`,
or `$config["input"]["delimiter"]`, or `$config["delimiter"]`, or `;` separator if none have been set.

It allows you to know if some rows are incomplete.

An example, let’s say you have the following csv files :

`~/products.csv` containing the following data :

|EAN|SHORT_DESC|PRICE (€)|UNIT|QTT|
|---|----------|---------|----|---|
|1231235689458|A fancy product|100.32|boxe|30|
|8956587747457|A special product|10000|unit|1|

`~/cmd.csv` contains the following data :

|EAN|TITLE|LONG_DESC|CATEG_1|CATEG_2|CATEG_3|
|---|-----|---------|-------|-------|-------|
|1231235689458|Fancy|A really fancy blue product.|Products|Fancy|Blue|
|2356854754445|Cool|A really cool pink product.|Products|Cool|Pink|

The diff tracker will create you two files :

`~/result/found_in_products_but_not_in_cmd.csv` with the following data :

|EAN|SHORT_DESC|PRICE (€)|UNIT|QTT|
|---|----------|---------|----|---|
|8956587747457|A special product|10000|unit|1|

**Important note :** by default, this product will be incomplete in the final dataset, as no corresponding data
have been found in `~/cmd.csv`. This behavior can be changed adding `row_filters` in
[input file configuration](#otherOptions).

And `~/result/found_in_cmd_but_not_in_products.csv` with the following data :

EAN|TITLE|LONG_DESC|CATEG_1|CATEG_2|CATEG_3|
|---|-----|---------|-------|-------|-------|
|2356854754445|Cool|A really cool pink product.|Products|Cool|Pink|

**Important note :** by default, this data will be ignored, as no corresponding data
have been found in `~/products.csv`. This behavior can be changed setting the `add_if_not_found` key to `true`
in [input file configuration](#otherOptions).

#### <a name="beforeCompilationRowChange">Changing rows before compilation</a>

Sometimes you need to change rows value just before it will be stored in the compiled file (for
resource consuming tasks involving fetching data from network for example). To dot it, you can define
a callable in `$config['input']['row_modifier']` that must accept an `array` as first parameter and return an
`array`.

```php
$config['input']['row_modifier'] = function(array $row):array{
	if(!isset($row["LONG_DESCRIPTION"]){
		//get online
		$row["LONG_DESCRIPTION"] = $fetchedData;
	}
	return $row;
};
```

#### <a name="mediasFolder">Specifying the medias folder</a>

You can define a directory in which the compiler will try to find medias to link to each item using the `$config["input"]["medias"]`
key. The directory can be relative or absolute.

Please see the [change input root folder section](#changeInputRootFolder) to know more about relative
path resolution.

#### <a name="inputFileConfiguration">Input files configuration</a>

To add a csv file to parse, please fill the `$config["input"]["files"]` array as follows :

```php
$config = [
	//...
	"input" => [
		//...
		"files" => [
			"an_arbitrary_identifier" => [
	"path" => "file.csv",
	"columns" => [ //... ]
			]
		]
	]
];
```

The `path` field is **required** and must contain a relative or absolute path to the file to parse.
For the relative paths resolution, please read [the previous section](#changeInputRootFolder).

The `columns` field is **required** and must be an array containing column name as index, and an array of column
params as value.

##### <a name="fileColumnsConfiguration">File columns configuration</a>

Each column is declared with its name as key, and an array of options.

The required `columns` field must declare at least one index and must be an array. You can let it empty
if you don't need a special configuration.

```php
$config = [
	//...
	"input" => [
		//...
		"files" => [
			"an_arbitrary_identifier" => [
				//...
				"columns" => [
					"EAN" => [ "not_empty" => true],
					"DESCRIPTION" => []
				],
				//...
			]
		]
	]
];
```

There is the complete list of available configuration keys available for each column :

|   Key   |         Type        |  Default   |                      Description                               |
|---------|---------------------|------------|----------------------------------------------------------------|
|not_empty|      `boolean`      |   false    |If set to true, a row where this column is empty will be ignored|
|default  | any OR `LAST_VALUE` |    null    |If set will replace any empty value by this value. ([see note about LAST_VALUE](#noteAboutLastValue))    |
|modifier |      `callable`     |    null    |If set will use the result of the callable to fill the cell. ([see note about this callable](#noteAboutModifierCallable))   |

<a name="noteAboutLastValue">Note about LAST_VALUE :</a> `LAST_VALUE` is a constant that allows you to use the value of the last row for this cell.
If no value was found before lets the column empty.

<a name="noteAboutModifierCallable"> Note about `modifier` callable :</a> The callable must accept a `string` as first argument
and should return a `string`.

##### <a name="otherOptions">Other options</a>

There is the list of other optional keys that can be defined for a specific file :

|      Key       |         Type        |  Default   |                      Description                               |
|----------------|---------------------|------------|----------------------------------------------------------------|
|add_if_not_found|      `boolean`      |   false    |If set to true, a row that hasn't been found (based on index) will be added to the dataset instead of being ignored. This option is useful to merge several files.|
|   row_filters  |     `callable[]`    |    null    |If set, if any filter return false, the row will be ignored. ([see not about filters callable](#noteAboutFiltersCallable))    |
|    indexes     |      `string[]`     |    null    |If set, will use only indexes that are listed. ([more about this in the indexes section](#indexes))   |
|    delimiter   |       `string`      | $config['delimiter'] | If set, will override the delimiter for all input files |

<a name="noteAboutFiltersCallable">Note about filters callable :</a> Each filter must accept an `array` as the first argument (the parsed row, ready to be adding to the compiler)
and return a `boolean`. `True` if the row must be kept, `false` otherwise.

**Note about the row `array` :** it contains all row values (after that all defined [modifiers](#fileColumnsConfiguration)
have been applied if any), in columns order and each value is indexed under the column name.
`$row["EAN"]`, for example, will return the current item EAN cell value.

#### <a name="indexes">Indexes</a>

An index is a column that the compiler will use to define two concepts :

- Line identity (uniqueness)
- Line validity (all lines that doesn't have a valid index will be considered as invalid, and ignored)

To do so, we must define the configuration array `$config["input"]["indexes"]` with, as key the
column name and an `array` of parameters, as follows :

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"EAN" => [ "validator" => "/^[\d]{13}$/" ]
		]
	]
];
```

**Note :** The `validator` field is required and can be :

- A valid full regexp pattern that will be passed to `preg_match`
- A callable that returns a boolean and takes the column value as the first argument, as demonstrated below.

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"STRANGE_INDEX" => [ "validator" => function(string $colValue) : bool{
				return is_int(strpos("FOO",$colValue));
			} ]
		]
	]
];
```

##### <a name="variableIndexes">Variable indexes</a>

Sometimes, indexes can vary across files. Let's say that in your main dataset, you have three columns
that can be used to identify a product :

- Your SKU
- The manufacturer SKU
- The product EAN

You need to set up a configuration like this :

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"EAN" => [ "validator" => "/^[\d]{13}$/" ],
			"SKU" => [ "validator" => "/^[A-Z0-9_-]$/" ],
			"MANUFACTURER_SKU" => [ "validator" => "/^[A-Za-z0-9_/@-]$/" ]
		]
	]
];
```

**Important note :** All indexes that appears across files MUST be declared there, even if they're not
defined in the first file. To concat several files, please see the [`"add_if_not_found"` configuration key section](#otherOptions) in
[input file configuration](#inputFileConfiguration)

###### <a name="differentIndexes">Different indexes in different files</a>

In some files, you may have only one of these identifiers to refer to a product, so you can define which
index or which indexes must be used to identify each rows :

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"EAN" => [ "validator" => "/^[\d]{13}$/" ],
			"SKU" => [ "validator" => "/^[A-Z0-9_-]$/" ],
			"MANUFACTURER_SKU" => [ "validator" => "/^[A-Za-z0-9_/@-]$/" ]
		],
		"files" => [
			"first" => [
				"path" => "first.csv",
				"columns" => [
					"EAN" => [ "not_empty" => true ],
					"SKU" => [ "not_empty" => true ],
					"MANUFACTURER_SKU" => [ "not_empty" => true ],
					//...
				]
				//...
			],
			"manufacturer" => [
				"path" => "manufacturer.csv",
				"indexes" => [ "MANUFACTURER_SKU" ],
				"columns" => [
					"MANUFACTURER_SKU" => [ "not_empty" => true ],
					//...
				]
			],
			"products_seo" => [
				"path" => "seo_infos.csv",
				"indexes" => [ "SKU" ],
				"columns" => [
					"SKU" => [ "not_empty" => true ],
					//...
				]
			]
		]
	]
];
```

**Important note :** In this example, the first file MUST contain all indexes in order to the compiler
to be able to link data across files, and all other files MUST contain at least one index.

**Note :** All indexes defined in a file configuration MUST be defined in `$config["input"]["indexes"]`

###### <a name="indexAdapters">Soften matching indexes method</a>

Sometimes, if a user generates a csv from a spreadsheet software like Excel, some SKU or other index can be
altered, or some references can be matched against other references.

To tell the compiler to soften and modify it indexes recognition method, you can set the `adapters` key
in any index declaration. An adapter can be

- a `string` where the substring `@INDEX` will be replaced by the current index value when the compiler
tries to retrieve a row.
- a `callable` that takes in the first argument a string value (the current index value) and return a string
that must be the index value modified.

For example, the following configuration will end up to match those indexes for the index value "2365sku" :

- 2365sku
- 2365SKU
- 02365sku

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"EAN" => [
				"validator" => "/^[\d]{13}$/",
				"adapters" => [
					"0@INDEX",
					function(string $indexValue):string{
						return strtoupper($indexValue);
					}
				]
			]
		]
	]
];
```

**Important note :** has you can see, adapters are not combined. In the previous example, if you
want to match '02365SKU' too, you must redefine the callable or add another one.

### <a name="outputConfiguration">Output configuration</a>

The `$config['output']` key allows you to configure the output generated by the compiler. The resulting
aggregated csv file will be built reading the compiled php file created during the input parsing process.

There will be all configuration keys that allows you to change the way the compiler will build is reports
too.

#### <a name="mediasDestination">Changing medias destination</a>

The `$config['output']['medias']` key allows you to define a directory in which all found medias will be
stored to simplify later import in another software. All file paths will be relative to this directory.

The directory will be fully created and can be a relative or absolute path. To learn more about relative
path resolution, please [read the following section](#changeOutputRootFolder).

Default value : `medias`.

#### <a name="includeMediasPath">Medias relative path</a>

The `$config["output"]["include_medias_path"]` key allows you to change the final medias path to include
the `$config["output"]["medias"]` path before.

Default value : `false`.

**Note :** this path will be relative nor absolute depending on `$config["output"]["medias"]` value.

#### <a name="absoluteMediasPaths">Making medias paths absolute</a>

The `$config["output"]["make_medias_paths_absolute"]` key allows you to make all relative medias paths
absolute adding a slash at the beginning.

Default value : `false`.

#### <a name="changeOutputRootFolder">Changing root folder</a>

In order to not forcing you to declare all paths in an absolute way, you can define a `$config["output"]["root"]` key
that will be used to resolve relative paths.

If you don't, relative paths will be resolved with the `$config['dir']` key value or in the csvCompiler.php
directory folder (`__DIR__`) if no `dir` key have been defined.

Whether you declare this key or not, you can always define absolute paths.

```php
$config = [
	//...
	"output" => [
		//...
		"root" => "/home/me"
	]
];
```

#### <a name="changeOutputDelimiter">Changing output files delimiter</a>

You can override the `$config['delimiter']` only for output configuration setting `$config['output']['delimiter']` key to a new
delimitation `char` or `string`.

**Note :** this delimiter will be used for all output files.

#### <a name="changeOutputInsideDelimiter">Changing output inside delimiter</a>

To automaticaly convert arrays in the final dataset, the default `csvCompiler` behaviour will be to use
`implode` with the `,` delimiter. You can override this by setting the `$config['output']['inside_delimiter']` key to a new
delimitation `char` or `string`.

#### <a name="trackDiffTemplate">Changing tracked differences between files naming conventions</a>

By default, the template for file names generated by the diff tracker is `found_in_@from_but_not_in_@too.csv`
where `@from` will be replaced by the source file where the compiler found a data that can't be linked
to an item found in the dataset, and the `@too` tag will be replaced by the source file where the compiler
can't find the data.

**Important note :** if you used the `add_if_not_found` options ([learn more about this](#otherOptions)), the
`@too` file will always be the first one. There is no way to link each difference in this special case,
since tracking diff this way implies a huge performance overhead.

You can change this template using the `$config['output']['diff_tracker_template']`.

#### <a name="compiledPHP">Changing or specifying the compiled php file name</a>

Sometimes you may want to change the compiler php file to feed the compiler with an other compiled source.

You can change the default compiled php file name and/or location using the `$config['output']['compiled']`.
The default value is `compiled.php`. To learn more about relative path resolution, please
[read the dedicated section](#changeOutputRootFolder).

#### <a name="replaceLines">Replacing one line by one or more</a>

Sometimes (as for Magento2.3 to set a list of images for a product), you need to split a data row in
several rows. To do this, you can assign a callable to the `$config['output']['line_replacer']`.

This callable must take as the first argument an `array` that is the formed dataset current row, and return an
`array` of rows :

```php
$config['output']['line_replacer'] = function(array $line):array{
	return [$line];
}
```

Each row will be added in the order they appear in the resulting `array`.

**Important note :** you can use this parameter to filter rows too, as if you return an empty `array`,
the row will not be added to the final file.

#### <a name="modifyFinalDataset">Modify the compiled dataset before colmuns map transformation</a>

Sometimes, you need to perform some operations in the dataset before the columns_map te be processed
over the compiled dataset. To do this, define a `callable` at `$config['output']['dataset-modifier']`.

This `callable` must accept an `array` as first parameter and return an `array` that must be the dataset
used for the column mapping.

##### <a name="relatedProductsExample">Related products example</a>

A usefull case for this is when you need to create a column that require the full dataset to be filled, as
detect related produtcs in a products list.

The below callable will be able to detect some related products and to prepare an `array` of indexed
relations that can be used by a `columns_map` entry to fill a `related_skus` column :

```php
$relatedMap = []; //the final map
$minSimilarity = 60; //in percent, the minimal similarity amount between two strings that is required to establish a 'related' relation between products
$similaritiesCache = "similarities-milan-otakee.map"; //the file
$clearSimilaritiesCache = false; //if true, will always restart computation

$config['output']['dataset_modifier'] => function(array $dataset) use (&$relatedMap,&$log,$minSimilarity,$similaritiesCache,$clearSimilaritiesCache):array{
	$similaritiesCache = __DIR__."/$similaritiesCache";
	if(file_exists($similaritiesCache)){
		if(!$clearSimilaritiesCache){
			try{
				$relatedMap = json_decode(file_get_contents($similaritiesCache),true);
				$log("Similarities map cache loaded.",OK);
				return $dataset;
			}catch(Error | Exception $e){
				$log("Error while trying to load similarities cache : $e",ERR);
			}
		}else{
			unlink($similaritiesCache);
			$log("Similarities cache cleared.",OK);
		}
	}else $log("No similarities cache file found. A new one will be built.");
	$log("Starting related product mapping (Please be patient, this operation can take few minutes)...");
	$relatedMap = array_flip(array_map(function($line){ return $line["REF"]; },$dataset));
	foreach($dataset as $nb=>$line){
		foreach($dataset as $line2){
			$index1 = $line["REF"]; $index2 = $line2["REF"];
			if($index1 === $index2) continue;
			if(!is_array($relatedMap[$index1])) $relatedMap[$index1] = [];
			if(!is_array($relatedMap[$index2])) $relatedMap[$index2] = [];
			$text1 = strtolower($line["TITLE"] ?? $line["SHORT_DESC"]);
			$text2 = strtolower($line2["TITLE"] ?? $line2["SHORT_DESC"]);
			$p1 = 0; $p2 = 0;
			similar_text($text1,$text2,$p1);
			similar_text($text2,$text1,$p2);
			$p = ($p1 + $p2)/2;
			if($p < $minSimilarity) continue;
			if(count($relatedMap[$index1]) < 10) $relatedMap[$index1][$index2]=$p;
			else{
				$min = min(array_values($relatedMap[$index1]));
				if($min < $p) foreach($relatedMap[$index1] as $k=>$v){
					if($v === $min){
						unset($relatedMap[$index1][$k]);
						$relatedMap[$index1][$index2] = $p;
						break;
					}
				}
			}
			if($p>$minSimilarity){
				if(count($relatedMap[$index2]) < 10) $relatedMap[$index2][$index1]=$p;
				else{
					$min = min(array_values($relatedMap[$index2]));
					if($min < $p) foreach($relatedMap[$index2] as $k=>$v){
						if($v === $min){
							unset($relatedMap[$index2][$k]);
							$relatedMap[$index2][$index1] = $p;
						}
					}
				}
			}
		}
		if($nb%10 === 0) $log("Similarity search processing : ($nb/".count($dataset).").");
	}
	$log("Related product map built up.",OK);
	file_put_contents($similaritiesCache,json_encode($relatedMap,JSON_PRETTY_PRINT));
	$log("Similarities cache file created ($similaritiesCache)",OK);
	return $dataset;
}

//then using the relatedMap in the `columns_map` output :
$config['output']['columns_map']['related_skus'] = function(array $row) use (&$relatedMap,&$config):string{
	return implode(
		$config["output"]["inside_delimiter"]??",",
		array_keys($relatedMap[$row["REF"]]??[])
	);
},
```

**Note :** as this kind of operation can be expansive, the above algorythm cache the final result
in a file, and try to load it. Usefull when you need to test several or generate several csv with
some variations.

#### <a name="columnMapping">Column mapping</a>

The `$config['output']['columns_map']` key allows you to define a map between the compiled columns
and the final one you want to appear in the final csv file.

Each key should be a column name to map in the compiled dataset, and value an `array` which options are listed below.

##### <a name="changeColumnName">Changing column name</a>

To change a column name, simply add a `name` option attribute in the column's options `array` :
`$config["output"]["columns_map"][ANY]["name"] = "new_name"`.

##### <a name="columnFormater">Formatting or changing column value</a>

To change a column value for a mapped column, add a `formater` attribute to the column's options `array`. This
MUST be a `callable` that accept any value as the first parameter and return a `string`, as follows :

```php
$config["columns_map"][ANY]["formater"] = function($value):string{
	return (string)$value;
};
```

##### <a name="columnCreation">Creating a new column</a>

Sometimes, you need to create a new column in the final generated csv file. To create a new one,
just add a new key in the `columns_map` and set it with a `callable` or a `scalar` instead of an `array`.

This callable MUST accept an `array` as the first parameter (the full dataset current row) and return a `string`, as follows :

```php
$config["output"]["columns_map"]["tax_amount"] = function(array $row):string{
	if(isset($row["COUNTRY"]) && $row["COUNTRY"] === "FRANCE") return 20;
	else return 5;
};
```

##### <a name="columnExclusion">Excluding a column</a>

Sometimes, you may need an additionnal column that must not be shown in the final csv file. To exclude
a column just before the output csv file will be written, add a new key in the `columns_map` and set it with
 an `array` that MUST contain the `exclude` key to true :

 ```php
 $config["output"]["columns_map"]["TEMP"] = [
 	"exclude" => true
 ];
 ```

#### <a name="includeUnmapped">Including unmapped columns in result</a>

If you want to keep unmapped columns as they're defined in the compiled dataset, set the
`$config["output"]["include_not_mapped_entries"]` to `true`.

**Note :** there is no way to skip some columns with this method. If you want to discard some columns
in the final generated file, you must let this option to `false` and define all columns in the `columns_map`.

#### <a name="stringConverter">Convert compiled values to string</a>

If you filled the compiled dataset with complex data that cannot be easily stringified, set a `callable` in the
`$config["output"]["string_converter"]` key. This `callable` must accept any value as the first argument, and return a `string`.

**Note :** please avoid using this kind of complex data in compiled datasets, since a csv file must not contain objects.
This option exists only for special edge cases that should stay exceptions.

#### <a name="mediasMapping">Medias mapping</a>

Since the `csvCompiler` was designed to aggregate csv files and link them to external resources,
there are some options to customize the final output.

##### <a name="changingMediasKey">Changing medias key in compiled php format</a>

If you want to change the medias key in the compiled php file, please set the `$config['output']['medias_key']`
to any `string` value. This can be useful if this name is already used as a column in other files.

##### <a name="excludingNoMediasItems">Excluding items without medias link</a>

You can exclude all rows that are not linked to at least one medias setting the
`$config['output']['remove_items_with_no_medias']` key to `true`. The default value is `false`.

##### <a name="noMediasReport">Enabling no medias report generation</a>

If you want to get a track of all rows that haven't been linked to at least one external resource,
set the `$config['output']['no_medias']` to a file name.

If the file name is a relative path, it will be resolved as `$config['output']['root']`. To learn more
about output relative path resolution, please read the [according section](#changeOutputRootFolder).

**Note :** if this option hasn't been defined, the report will not be generated.

##### <a name="noMediasFallback">Defining a no medias found fallback</a>

In case you want to load an external resource in another way than in the specified folder, you can
define a fallback function that will try to retrieve external resources in another way.

The most useful case for this feature is to download the picture online, or from a distant server.

To do it, set a `callable` to the `$config['output']['medias_not_found_fallback']` key. This `callable`
must accept an `array` as the first argument that is the current dataset row and returns an `array` of `string`
that are local paths to all resources linked to the current item, as follows :

```php
$config['output']['medias_not_found_fallback'] = function(array $row):array{
	//do some search on a catalog website for example
	return $mediasList;
};
```

**Note :** if no medias is found, you must return an empty `array`, or a default array with a default medias list.

##### <a name="mediasPathsFiltering">Filtering found medias paths</a>

You can filter found medias path setting a `callable` to the `$config['output']['medias_filter']` key. The `callable` MUST
accept two arguments, and return an `array` of `string` that are the resulting file paths list :

- the first is an `array` that is the medias list
- the second is an `array` that is current dataset row

```php
$config['output']['medias_filter']=function(array $paths, array $line):array{
	return [];
};
```

##### <a name="mediasFileRenamer">Renaming medias files</a>

You can rename your medias files by setting a `callable` to the `$config['output']['medias_file_renamer']` key. The `callable` MUST
accept one argument, and return a `string` that is the new file name :

```php
$config['output']['medias_file_renamer'] = function(string $oldName):string{
	return strtolower($oldName);
};
```

##### <a name="mediaFileAdapter">Soften the medias mapping method</a>

Sometimes, medias names in directory has mistakes or may need to soften rules to match against the final dataset.
As for the [soften indexes matching features](#indexAdapters), you can define a `callable` in
`$config['output']['medias_file_adaper']` that will return an `array` of file name variations to match.

**All returned strings can be REGEXP, since the compiler will test them against files name with a regexp
with the `#` delimiter, as follows : `"#.*$path.*#i"`.**

As you can see, the regexp automatically set the `i` flag that means case insensitive.

The most obvious usage for this is the underscore (`_`) substitution by
a space char, that can be expressed as this :

```php
$config['output']['medias_file_adaper'] = function(string $path):array{
	return [ substr("_"," ",$path) ];
};
```

The above example will match the above pictures, for the index value `0325S_720p` :

- 0325S_720p
- 0325S_720P
- 0325s_720P
- 0325s_720p
- 0325S 720p
- 0325S 720P
- 0325s 720P
- 0325s 720p

**Important note :** if there is many files and rows, this kind of matching can increase significantly
the processing time.

**Important note 2 :** if you defined some soften index matching, you don't need to redefine them there, as
these softer rules will already be used by the compiler to trying to find any medias file. So, your
`medias_file_adapter` will receive the full index and weakened indexes, if any.

As an example, if you set up indexes like this :

```php
$config = [
	//...
	"input" => [
		//...
		"indexes" => [
			"EAN" => [
				"validator" => "/^[\d]{13}$/",
				"adapters" => [
					"0@INDEX",
					function(string $indexValue):string{
						return strtoupper($indexValue);
					}
				]
			]
		]
	]
];
```

The compiler will already search for these strings in the file name for "2365s_b" :

- 2365s_b
- 2365S_B
- 02365s_b

So, with an adapter like the following one :

```php
$config['output']['medias_file_adaper'] = function(string $path):array{
	return [ substr("_"," ",$path) ];
};
```

You can achieve to test all those combinations :

- 2365s_b
- 2365S_B
- 2365S_b
- 2365s_B
- 02365s_b
- 02365S_B
- 02365S_b
- 02365s_B
- 2365s b
- 2365S B
- 2365S b
- 2365s B
- 02365s b
- 02365S B
- 02365S b
- 02365s B

#### <a name="logsConfiguration">Logs</a>

A `$log` anonymous function is available in your configuration file, since the config file is required
after its declaration.

To use it, just do as follows :

```php
$custom = function() use ( &$log ){
	$log("My log info");
};
```

This anonymous function accepts two arguments :

- The first is the text to be logged.
- The last (optional) is an array (see [how to customize your logs](#logsLineParams))

You can configure the `csvCompiler` to output all logging info into a file, and even change the log
templates and appearance.

##### <a name="fileLogging">Enable file logging</a>

To enable file logging, just add this configuration key into the `$config["output"]["logs"]` key,
as following :

```php
$config = [
	//...
	"output" => [
		//...
		"logs" => [
			"path" => "/path/to/logs.log"
		]
	]
];
```

**Note :** You can define an absolute or relative path. If relatives, it will be resolved as `$config["output"]["root"]`
(see [more about output path resolution](#changeOutputRootFolder)).

**Note 2:** You can disable `STDOUT` prompting by adding the `-quiet` argument on `csvCompiler` call.
(see [more about command line arguments](#commandArguments))

##### <a name="logsTemplate">Changing logs template</a>

To change the log template, you don't have other choices to redefine the `LOG_TEMPLATE` constant into the
`csvCompiler.php` file :

```php
//...
define("LOG_TEMPLATE","\e[@colorm[@date] [@type] @message\e[0m\n");
//...
```

You can ignore some replacement keys to fit your needs, but they're the only keys available. The date format
can't be changed in another way than changing the $log function definition.

For example, this pattern will disable the bash formatting info :

```php
//...
define("LOG_TEMPLATE","[@date] [@type] @message\n");
//...
```

##### <a name="logsLineParams">Changing logs line appearance</a>

If you want to change the bash formatting (colors) of messages logged into your console, you can change
the following constants :

- `LOG` : Default level (default : default bash color)
- `OK` : Success level (default : text color green)
- `WARN` : Warning level (default : text color orange)
- `ERR` : Error level (default : text color red)
- `FATAL` : Fatal error level (must stop the script execution) (default : red background)

You can either create your own constants for your personal use. They must be defined as follows :

```php
define("CUSTOM",["text" => "CUSTOM LOG", "color" => 44 ]);
```

The `text` key is the text displayed to specify the log level (`@type` in the `LOG_TEMPLATE`)

The `color` key is the bash color code to change log line appearance (`@color` in the `LOG_TEMPLATE`). To learn
more about bash coloring, please follow [this link](https://misc.flogisoft.com/bash/tip_colors_and_formatting)

# Contribution

Feels free to contribute !