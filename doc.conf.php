<?php
//all anonymous functions can use &$downloadRemoteFile and/or &$log functions, since the config
//file will be required after their declaration.
$config = [
	//base working dir in wich files and folder will be created by default
	"dir" =>"/home/result",
	//Column sepearator in files
	"delimiter" => "|",
	//INput params
	"input" => [
		//optionnal. Root folder to resolve relative paths. If not given, __DIR__ will be used
		"root" => "/home/",
		//override the $config["delimiter"] for all input files
		"delimiter" => ";",
		//folder containing medias files. File names must contain and INDEX value, if not, no file
		//will be mapped to any line, as the compilator will not find them.
		"medias" => "ftp",
		//if true, create report file with missing lines in all files where they're not found (separated).
		//May leads to n generated file (where n is the total number of files).
		"track_diff" => true,
		//INdexes used to aggregate file lines
		"indexes" => $indexesDefinitions = [
			// EAN
			"EAN" => [
				//validator -> full regexp (will be used as this in preg_match)
				//or callable that will return a boolean and accept a string as argument
				"validator" => "/^[\d]{13}$/",
				//list of adapters to check for other value. Can be callables or string where the
				//substring @INDEX will be replaced by the not found index value.
				"adapters" => [
					"0@INDEX",
					function(string $indexValue):string{ return strtoupper($indexValue); }
				]
			]
		],
		//Optionnal. Default : $config["input"]["indexes"]
		//if given, will check rows unicity with the given rules.
		//This options format follow exactly the $config["input"]["indexes"] definition
		"row_unicity_check_indexes" => $indexesDefinitions,
		//If specified, allow you to modify a row just before it will be stored in the compiled
		//php file.
		"row_modifier" => function(array $row):array{ return $row; },
		//File list to parse
		"files" => [
			//Name of the entry
			// /!\ the first file must be the one with the most lines, in order to produce readable
			// report when datasets doesn't contain enough data, and not the same number of lines
			"cmd" => [
				//path to the file (absolute OR relative to input/root or __DIR__)
				"path" => "cmd.csv",
				//Optionnal. If not defined, $config["input"]["indexes"] will be used to define indexes
				//for the current file.
				//Array of column name that should be used as index. /!\ WARNING : all indexes name must be defined
				//in $config["input"]["indexes"]
				"indexes" => ["REF"],
				//Optionnal. If set to true, all lines that havn't been found in previous files (if any)
				//will be added to the final dataset. The default behaviour is to not add incomplete
				//data, but sometimes it's usefull to merge some identic splited files removing
				//duplications.
				"add_if_not_found" => false,
				//optionnal. If specified, will apply filters based on row. Must be an array of callable
				//that takes an array as first argument and return a boolean.
				//if boolean is false, the row will be ignored.
				"row_filters" => [
					function(array $row):bool{ return true; }
				],
				//column list (order mater)
				"columns" => [
					//all column params are optionnal, but each key must be associated to an array,
					//even if no option (ex : "A_KEY" => [],... )
					"COL_NAME" => [
						//can be any value OR the constant LAST_VALUE thaht will pick the last defined
						//value for this column in precedent lines OR null if never encountered
						"default" => "scalar_default_value",
						//if true, will print errors when this col value is empty
						"not_empty" => false,
						//a callable to modify the column value
						"modifier" => function(string $value):string{ return strtoupper($value); }
					]
				]
			]
		]
	],
	"output" => [
		//delimiter for columns in csv files
		"delimiter" => ";",
		//delimiter for array conversion into columns
		"inside_delimiter" => ",",
		//optionnal. Relative path will be resolved as dir if not given.
		"root" => "/home/result",
		//name of the compiled php file (intermediary format used to generate the final file)
		"compiled" => "compiled.php",
		//Optionnal. Must be a callable that takes a dataset as the first argument and return a dataset
		//allows you to perform actions that need a global access to the dataset, just before the columns_map
		//transformations.
		"dataset_modifier" => function(array $dataset):array{ return $dataset; },
		//template name for tracked diff in files
		"track_diff_template" => "found_in_@from_but_not_in_@too.csv",
		//optionnal, allow you to replace one line with one or more lines
		//takes the line as associative array in arg, and return all lines in an array
		//if the array is empty, the line is removed.
		//New lines are added in the same order as is the returned array
		//Be cautious : this action will be called on each line and can increase significantly the
		// execution time
		//The second argument for the callable is the line before mapping, the firs the line
		//mapped
		//the given line is the resulting line after beeing parsed by all columns_mapper options.
		"line_replacer" => function(array $line):array{
			return [$line];
		},
		//mapping between the intermediary format of compiled file to the destination file
		"columns_map" => [
			// Specify all columns that must appears in the final dataset
			// order matters. All entries must be arrays or callable, even if no option are specified
			// ex : "A_COLUMN" => []
			// if a column is an additionnal column that rely on no existing column in compiled dataset,
			// the argument must be a scalar or a callable of this format : function(array $line):string{}
			// ex : "SHIPPING" => function(array $line):string{
			//      return ($line["PRICE"] > 30) ? "free shipping" : "expensive shipping"
			// }
			"EAN" => [
				//optionnal, renamme the column
				"name" => "CUSTOME NAME",
				//optionnal, formater to change the value
				"formater" => function(string $val):string{ return strtolower($val); }
			]
		],
		//optionnal.
		//If true, will add all columns that havn't an entry in columns_mapper, without any modification
		"include_not_mapped_entries" => false,
		//optionnal.
		//if a callable is given, it will be called to stringify values contained in compiled lines.
		"string_converter" => function($value):string{ return (string) $value; },
		//path to the final filename
		"generated_filename" => "products_for_magento.csv",
		//final matching medias location
		"medias" => "pub/media/import",
		//include $config['output']['medias'] before each file to import
		"include_medias_path" => false,
		//will add a trailing slash before each medias.
		"make_medias_paths_absolute" => false,
		//optionnal. medias key in resulting compiled dataset.
		"medias_key" => "MEDIAS",
		//optionnal. If used, will track all items that havn't been linked to one or more
		//medias.
		"no_medias" => "no_medias_found.csv",
		//optionnal. If defined, allow to define a fallback function to try to retrieve items picture
		//in another way. (http/curl request on remote DB/file hoster)
		"medias_not_found_fallback" => function(array $line):array{ return $line; },
		///optionnal. If defined, will apply a filter to transform the paths array
		// while mapping files to items (allow you to exclude some file based on their name
		// ,directory, size... (low quality, high quality...)
		"medias_filter" => function(array $paths, array $line):array{
			return [];
		},
		//optionnal. If defined, will apply an adapter to try to match items and pictures, on file names
		//Must define an array of callables that takes a filename as argument and return an array of filenames
		//to try to match to. All callable results will be concatened.
		"medias_file_adapter" => [
			function(string $name):array{
				return [str_replace("_","",$name)];
			}
		],
		//Optionnal. If true, will remove from the final list all items that havn't been linked to
		// one or more medias.
		"remove_items_with_no_medias" => false,
		//Logs config
		"logs" => [
			//optionnal. null | string. If string, all logs will be stored in the specified file.
			//if relative, will be resolved as output/root or base working dir.
			"path" => "csvCompiler.log",
		]
	]
];