#!/usr/bin/php -q
<?php
/**
 * @license GNU General Public License v3.0 ( https://choosealicense.com/licenses/gpl-3.0/ )
 * @author Jordan Breton ( Agence web Bee-color : https://bee-color.fr )
 * @repository https://framagit.org/Ariart/csvCompiler.git
 * @version 1.0
 * @date 01/06/2019
 */
define("START_TIME",microtime(true));
//log constants
define("WARN",[ "color" => 33, "text" => "WARN" ]); //33 : orange
define("LOG", [ "color" => 0, "text" => "INFO" ]);//0 : default
define("ERR", [ "color" => 31, "text" => "ERR" ]);//31 : rouge
define("FATAL", [ "color" => 41, "text" => "FATAL ERROR" ]);//41 : beackground rouge
define("OK", [ "color" => 32, "text" => "SUCESS"]);//32 : vert
define("LOG_TEMPLATE", "\e[@colorm[@date] [@type] @message\e[0m\n");

//SPECIAL_PROCESSING_CONSTANTS
define("LAST_VALUE","--@@LAST_VALUE@@--");
define("MEDIAS_KEY","MEDIAS");

//will be overwritten
$config = [];
$dir = "/tmp/csvCompiler";
$logFile = null;

/**
 *Resolve a path. If $path is relative, and $root given, $path become $root/$path.
 * If $root is not specified and $path is relative, $path become __DIR__/$path
 * @param string      $path Path to resolve
 * @param string|null $root Absolute root path
 * @return string path resoleved
 */
function resolvePath(string $path,string $root=null):string{
	if(strpos($path,"/")!==0){
		if($root) return "$root/$path";
		else return __DIR__."/$path";
	}else return $path;
}

//Reading argv for command interpretor
$scriptName = $argv[0];
$cmdArgs = array_slice($argv,1);

$clean = false;
$quiet = false;
$confFile = null;
$compileOnly = false;
$current = array_shift($cmdArgs);
while($current){
	switch($current){
		case "-clean" :
			$clean = true;
			break;
		case "-quiet" :
			$quiet = true;
			break;
		case "--compile-only" :
			$compileOnly = true;
			break;
		case "-conf" :
			$arg = array_shift($cmdArgs);
			if(is_null($arg)) throw new InvalidArgumentException(
					"a path must be specified after the -conf arg !"
			);
			$configFile = resolvePath($arg);
			if(is_file($configFile)) $confFile = $configFile;
			else throw new InvalidArgumentException("File not found : $configFile");
			break;
		default :
			throw new InvalidArgumentException("Invalid argument : $current");
	}
	$current = array_shift($cmdArgs);
}

$log = function(string $message, array $params=LOG) use (&$logFile, $quiet){
	$handles = [];
	if(!$quiet) $handles[] = STDOUT;
	if(is_resource($logFile)) $handles[] = $logFile;
	foreach($handles as $h) fwrite($h,str_replace(
		["@color", "@date", "@type", "@message"],
		[
				$params["color"],
				\DateTime::createFromFormat('0.u00 U',microtime())
					->setTimezone(new \DateTimeZone(date_default_timezone_get()))
					->format("d-m-Y H:i:s.u"),
				$params["text"],
				$message
			],
		LOG_TEMPLATE
	));
};

/**
 * Download the file pointed out by $url into $dest
 *
 * @param string        $url              URL to the remote file
 * @param string        $dest             File destination
 * @param callable|null $curlOptParameter a callable that accept as first argument the curl ressource
 *                                        and return a curl ressource (can be the same) or null.
 * @return null|string
 */
$downloadRemoteFile = function(string $url, ?string $dest = null, ?callable $curlOptParameter=null):?string{
	if($dest) $fp = fopen ($dest, 'w+');// open file handle
	else $fp = null;

	$ch = curl_init($url);
	if(is_resource($fp)) curl_setopt($ch, CURLOPT_FILE, $fp);// output to file
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 1000);// some large value to allow curl to run for a long time
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	if(is_callable($curlOptParameter)) $ch = $curlOptParameter($ch) ?? $ch;
	if(!is_resource($fp)) curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$res = curl_exec($ch);

	curl_close($ch);// closing curl handle
	if(is_resource($fp)) fclose($fp);// closing file handle
	else return $res;
	return null;
};

//Change the default error handler to throw error on warnings too.
set_error_handler(function(string $errno, string $errstr, string $errfile, string $errline) use (&$log){
	$log(new ErrorException($errstr, 0, $errno, $errfile, $errline),FATAL);
	exit(1);
},E_ALL);

try{

//Checking required config file have been specified
if(!$configFile){
	$log("A conf file must be specified (usage : $scriptName -conf /path/to/conf.php ).",FATAL);
	exit(1);
}
require $configFile;

//logger configuration
$defaultLog = LOG;
$defaultLogTemplate = LOG_TEMPLATE;
$dir = resolvePath($config["dir"] ?? $dir);
if(!is_dir($dir)) mkdir($dir,0777,true);

$logFile= (isset($config["output"]["logs"]["path"]) && is_string($config["output"]["logs"]["path"]))
	? fopen(resolvePath($config["output"]["logs"]["path"],$dir),"w")
	: null;

//Utils functions for data processing

/**
 * Return a formated row (line) if the given one is valid, null otherwise
 *
 * @param array      $line    row (line) to parse
 * @param array      $options parsing options (@see config["input"]["files"])
 * @param string     $fileRef file reference for logging (default : filename:lineNumber)
 * @param string     $delimiter delimiter for row spliting
 * @param array|null $lastLineValues last line values
 * @return array|null
 */
$checkAndParseLine = function(
	array &$line,
	array &$options,
	string $fileRef,
	string $delimiter,
	?array $lastLineValues
) use (&$log) : ?array{
	$i = 0;
	$res = [];
	foreach($options as $name=>$opts){
		$val = $line[$i] ?? null;
		$val = trim($val);
		if(!empty($val)){ //if we have a value
			//if a callable have been defined to modify a value, call it
			if(isset($opts["modifier"]) && is_callable($opts["modifier"]))
				$val = $opts["modifier"]($val);
			$res[$name] = $val;
		}else if($opts["not_empty"] ?? false){ //if conf required non empty, return null and log error
			$log("$fileRef : Required column N°$i ($name) not found or empty ["
			     .implode("$delimiter",$line)."]", WARN
			);
			return null;
		}else if(isset($opts["default"])){ //use the default value if defined
			if(is_callable($opts["default"])) $res[$name] = $opts["default"]($val);
			else if($opts["default"]===LAST_VALUE && isset($lastLineValues[$name])) $res[$name] = $lastLineValues[$name];
			else $res[$name] = $val;
			//if a default value and a modifier are both defined, apply the modifier.
			if(isset($opts["modifier"]) && is_callable($opts["modifier"]))
				$res[$name] = $opts["modifier"]($res[$name]);
		}else $res[$name] = null; //set the column value to null
		$i++;
	}
	return $res;
};

	/**
	 * Remove duplacated items. Duplicated items are identified thanks to $indexes params that must
	 * follow the same structure as $config["input"]["indexes"]. Keep keys.
	 * To be considered as equal, all indexes must not be found in the building cache.
	 * If any is missing, the row will be inserted.
	 * If a row is find, the function will keep the one with the most columns.
	 *
	 * @param array $dataset Dataset to process
	 * @param array $indexes Indexes for unicity check
	 * @return array Cleaned dataset
	 */
function removeDuplicates(array $dataset, array $indexes):array{
	$res = [];
	$indexMatching = [];
	$cleanedData = array_values($dataset);
	foreach($cleanedData as $kline => $line){
		$found = false;
		foreach($indexMatching as $k => $indexesToTry){
			$matches = 0;
			foreach($indexesToTry as $indexKey => $indexValues){
				if(isset($line[$indexKey])){
					if(count(array_intersect($indexValues,[$line[$indexKey]])) > 0){
						$matches++;
					}
				}
			}
			if($matches === count($indexesToTry)){
				$found = true;
				$alreadyAddedLine = $res[(string)$k];
				foreach($line as $key => $value){
					if(empty($alreadyAddedLine[$key]) && !empty($value))
						$res[(string)$k][$key] = $value;
				}
			}
		}
		if(!$found){
			$tmp = [];
			foreach($indexes as $indexKey => $indexOpts){
				$tmp[$indexKey] = [];
				if(isset($line[$indexKey])){
					$tmp[$indexKey][] = $line[$indexKey];
					if(isset($indexOpts["adapters"])){
						foreach($indexOpts["adapters"] as $adapter){
							if(is_string($adapter)) $tmp[$indexKey][] = str_replace(
								"@INDEX",$line[$indexKey],$adapter
							);
							else if(is_callable($adapter)) $tmp[$indexKey][] = $adapter($line[$indexKey]);
						}
					}
				}
			}
			$indexMatching[(string) $kline] = $tmp;
			$res[(string) $kline] = $line;
		}
	}
	return array_values($res);
}

//Starts here
$compiledFile = resolvePath($config["output"]["compiled"]??"compiled.php", $config["output"]["root"] ?? $dir);
if(!is_dir(dirname($compiledFile))){
	$log("Creating ".dirname($compiledFile)."...");
	mkdir(dirname($compiledFile),0777,true);
}

if($clean){
	$log("Clean mode enabled.");
	exec("rm -rf \"$dir\" && mkdir -p \"$dir\"");
	$log("$dir cleaned");
}

$inputRoot = $config["input"]["root"] ?? null;
//Check optionnal medias folder validity
$medias = $config["input"]["medias"] ?? null;
if($medias){
	$medias = resolvePath($medias,$inputRoot);
	if(!is_dir($medias)) mkdir($medias,0777,true);
	if((count(scandir($medias)) == 2)){
		$log("$medias not found or empty !",WARN);
	}else $log("$medias found and ready.",OK);
}else $log("No medias folder specified.",WARN);

$mediasFolder = $config["output"]["medias"] ?? "medias";
$storeMediasFolder = resolvePath($mediasFolder,$config["output"]["root"] ?? $dir);

if(!file_exists($compiledFile)){
	$log("No $compiledFile found, a new one will be generated.", WARN);
	$log("Checking files...");

	//Check all files
	foreach($config["input"]["files"] as $name => $options){
		$config["input"]["files"][$name]["path"] = $tpath = resolvePath($options["path"],$inputRoot);
		if(!file_exists($tpath) && is_readable($tpath)){
			$log("$tpath not found or permission denied !", FATAL);
			exit(1);
		}else $log("$tpath found and ready.",OK);
	}

	$rowModifier = $config["input"]["row_modifier"] ?? null;
	if(!is_null($rowModifier) && !is_callable($rowModifier)){
		$log("\$config['input']['row_modifier'] MUST be a callable !",FATAL);
		exit(1);
	}

	//begin
	//will contain all data with no index. Each line will be indexed in $indexed tab.
	$data = [];
	$indexed = [];
	$parsedList = [];
	$uniqFoundList = [];
	$uniqNotFoundList = [];
	$indexes = $config["input"]["indexes"] ?? [ "EAN" => [ "validator" => "/^[\d]{13}$/"] ];
	$indexesOptionsForUniqueCheck = $config["input"]["row_unicity_check_indexes"] ?? $indexes;
	//prepare the indexed tab
	foreach($indexes as $name=>$options){ $indexed[$name] = []; }
	$nbFileParsed = 0;
	foreach($config["input"]["files"] as $name => $options){
		$delimiter = $options["delimiter"] ?? $config["input"]["delimiter"] ?? $config["delimiter"] ?? ";";
		$filePath = $options["path"];
		$fileIndexes = array_flip($options["indexes"] ?? []);
		if(count($fileIndexes) === 0) $fileIndexes = $indexes;
		else foreach($fileIndexes as $indexName=>$k){
			if(!isset($indexes[$indexName])){
				$log("Error in configuration for file $name ($filePath) : $indexName "
				     ."havn't been defined in \$config[\"input\"][\"indexes\"]",FATAL);
				exit(1);
			}
			$fileIndexes = $indexes[$indexName];
		}
		$colmuns = $options["columns"] ?? [];
		$rowFilters = $options["row_filters"] ?? [];
		$applyFiltersToRow = function(array $row) use ($rowFilters):bool{
			foreach($rowFilters as $filter){
				if(!$filter($row)) return false;
			}
			return true;
		};
		$addLineEvenIfNotFound = $options["add_if_not_found"] ?? false;
		$line = 0; //csv file current line
		$errors = 0; //error lines count
		$ignored = 0; //ignored lines count
		$missingLineAdded = 0; //missing line added thanks to the add_if_not_found file configuration
		$indexCorrection = 0; //corrected indexes values (see $config["input"]["indexes"][@indexName]["adapters"]
		$foundList = []; //all lines that have been found in other files
		$notFoundList = []; //all lines that have not been found in other files
		$lastLineValues = null;
		$parsedList[$name] = [];

		$log("Start parsing $name (location : $filePath, delimiter : $delimiter)...");
		$handle = fopen($filePath, "r");
		while(!feof($handle)){
			$l = rtrim(fgets($handle), "$delimiter\t\n\r\0\x0B");
			//if current line is empty, skip and continue
			if(empty($l)){
				$log("$filePath:$line : empty line skipped",WARN);
				$ignored++;
				$line++;
				continue;
			}
			$tmp = explode($delimiter,$l);
			$formatedLine = $checkAndParseLine(
				$tmp,$colmuns,"$filePath:$line",$delimiter, $lastLineValues
			);
			if(!$formatedLine){
				$ignored++;
				$line++;
				continue;
			}
			if(!$applyFiltersToRow($formatedLine)){
				$log("Row discarded by specific file row filters : $l",WARN);
				$ignored++;
				$line++;
				continue;
			};
			if(!$lastLineValues) $lastLineValues = $formatedLine;
			else foreach($formatedLine as $k=>$v){
				if(!empty($v)) $lastLineValues[$k] = $v;
			}
			$opts = null;
			$found = false;
			foreach($fileIndexes as $indexName => $indexOpts){
				if(isset($formatedLine[$indexName])){
					$opts = $formatedLine[$indexName];
					$validator = $indexOpts["validator"];
					if(is_string($validator) && !preg_match($validator,$opts)
						|| is_callable($validator) && !$validator($opts)){
						$log("$filePath:$line : invalid index $indexName value '$opts'",ERR);
						$errors++;
						$line++;
						break;
					}

					$found = false;
					if($nbFileParsed === 0){
						$data[] = $formatedLine;
						$indexed[$indexName][(string)$opts] = &$data[count($data)-1];
						$found = true;
					}else{
						if(isset($indexed[$indexName][(string)$opts])){
							foreach($formatedLine as $k=>$v){
								$indexed[$indexName][(string)$opts][$k] = $v;
							}
							$found = true;
						}else if(isset($indexOpts["adapters"])){
							foreach($indexOpts["adapters"] as $adapter){
								$searchIndex = null;
								if(is_callable($adapter)) $searchIndex = $adapter($opts);
								else $searchIndex = str_replace("@INDEX",$opts,$adapter);
								if(isset($indexed[$indexName][(string)$searchIndex])){
									$found = true;
									$formatedLine[$indexName]=$searchIndex;
									$log("$filePath:$line : item index correction $opts -> $searchIndex",WARN);
									$opts = $searchIndex;
									foreach($formatedLine as $k=>$v){
										$indexed[$indexName][(string)$opts][$k] = $v;
									}
									$indexCorrection++;
									break;
								}
							}
						}
					}
					$parsedList[$name][(string)$opts] = $formatedLine;
					if(!$found && $addLineEvenIfNotFound){
						$data[] = $formatedLine;
						$indexed[$indexName][(string)$opts] = &$data[count($data)-1];
						$found = true;
						$log("$filePath:$line : missing item '$opts' in previous files have been "
						     ."added to the final dataset accordingly to your configurations.",
						     WARN
						);
					}
					if($found){
						if(!isset($foundList[$name])) $foundList[$name]=[];
						if(!isset($foundList[$name][$indexName])) $foundList[$name][$indexName]=[];
						$foundList[$name][$indexName][(string)$opts] = $formatedLine;

					}else{
						if(!isset($notFoundList[$name])) $notFoundFile[$name]=[];
						if(!isset($notFoundFile[$name][$indexName])) $notFoundFile[$name][$indexName]=[];
						$notFoundList[$name][$indexName][(string)$opts] = $formatedLine;
						$log("$filePath:$line : unknown product $opts ignored (not present in previous files)",ERR);
					}
				}
			}
			$line++;
		}
		fclose($handle);
		if($nbFileParsed > 0){
			if(isset($foundList[$name]))
				$uniqFoundList[$name] = removeDuplicates(
					array_unique(
						array_values(array_merge(...array_values($foundList[$name]))),
						SORT_REGULAR
					),
					$indexesOptionsForUniqueCheck
				);
			if(isset($notFoundList[$name])){
				$notFoundTemporaryList = removeDuplicates(
					array_unique(
							array_values(array_merge(...array_values($notFoundList[$name]))),
							SORT_REGULAR),
					$indexesOptionsForUniqueCheck
				);
				$notFoundReport = ", ".count($notFoundTemporaryList)
					." not found items in previous files";
				$uniqNotFoundList[$name] = $notFoundTemporaryList;
			} else $notFoundReport = "";
			$addReport = ", $indexCorrection corrected indexes$notFoundReport";
		}else $addReport = "";
		if($missingLineAdded > 0) $addReport.=", $missingLineAdded missing lines in previous files added";
		$log("$filePath parsed ($line lines readed, $ignored lines skipped, $errors invalid item index format$addReport)",OK);
		$nbFileParsed++;
	}
	$data = removeDuplicates(
		array_values(array_unique($data,SORT_REGULAR)),
		$indexesOptionsForUniqueCheck
	);

	$noMediasList = [];
	if($medias){
		$log("Start mapping medias to items...");
		$log("Getting file list...");
		$mediasNotFoundFallback = $config["output"]["medias_not_found_fallback"] ?? function(array $row){
			return [];
		};
		$mediasFileAdapters = $config["output"]["medias_file_adapter"] ?? [function(string $name):array{
			return [$name];
		}];
		$filter = $config["output"]["medias_filter"] ?? null;
		if(!is_callable($filter)) $filter = null;
		$fileList = [];//get all files and parse array with regexp or use strpos not bool
		exec("find \"$medias\" -type f -name \"*\"",$fileList);
		$log(count($fileList)." files found. Trying to map them to existing items...");
		$noMediasList= [];
		$tmpFileAdaptersCache = [];
		foreach($data as $k=>$v){
			$matchingMedias = []; $opts=null; $indexesTry = [];
			foreach($indexes as $indexName => $indexOpts){
				if(isset($v[$indexName])){
					$opts = $v[$indexName];
					$indexesTry[] = $opts;
					$tmp = [];
					if(isset($v[$indexName])) foreach($fileList as $f){
						if(preg_match("#.*$opts.*#i",$f))
							$matchingMedias[] = $f;
						foreach($indexOpts["adapters"]??[] as $adapter){
							if(is_callable($adapter)) $adapter = $adapter($opts);
							else $adapter = str_replace("@INDEX",$opts,$adapter);
							if(!isset($tmpFileAdaptersCache[$f])){
								$mediasToParse = [];
								foreach($mediasFileAdapters as $fileNameAdapter){
									$mediasToParse[] = $fileNameAdapter($f);
								}
								$tmpFileAdaptersCache[$f] = $mediasToParse = array_merge(...$mediasToParse);
							}else $mediasToParse = $tmpFileAdaptersCache[$f];
							foreach($mediasToParse as $mediaFileName){
								if(preg_match("#.*$adapter.*#i",$mediaFileName)){
									$matchingMedias[] = $f;
								}
							}
						}
					}
				}
			}
			$matchingMedias = array_values(array_unique($matchingMedias));
			if(count($matchingMedias)===0) $matchingMedias = $mediasNotFoundFallback($v);
			if($filter) $matchingMedias = $filter($matchingMedias,$data[$k]);
			$data[$k][MEDIAS_KEY] = $matchingMedias;
			if(count($matchingMedias) === 0){
				$log("No medias found for item $k : '".implode(",",$indexesTry)."'.",ERR);
				$noMediasList[]=$v;
			}
		}
		$noMediasList = removeDuplicates($noMediasList,$indexesOptionsForUniqueCheck);
		$log("Medias successfully mapped (no medias for ".count($noMediasList)."/".count($data)." items).",OK);
	}

	if($rowModifier){
		$log("Applying row_modifier to dataset...");
		foreach($data as $k=>$line){
			$data[$k] = $rowModifier($line);
		}
		$log("row_modifier applyed",OK);
	}

	$log("Creating $compiledFile...");
	$hparsed = fopen($compiledFile, "w");
	$dontKeepNoMediasItems = $config["output"]["remove_items_with_no_medias"] ?? false;
	fwrite($hparsed,"<?php\n");
	fwrite($hparsed,"return [\n");
	$i = 0;
	foreach($data as $code=>$tab){
		foreach($tab as $k=>$v){
			if(is_string($v)) $tab[$k] = str_replace("\"","''",$v);
		}
		$line = "[";
		$isNoMediaItem = false;
		foreach($tab as $k=>$v){
			$replace = null;
			if(empty($v)){
				if($k===MEDIAS_KEY) $isNoMediaItem = true;
				continue;
			}
			if(is_array($v)){
				$replace = "[\"".implode("\",\"",$v)."\"]";
			} else if($k===MEDIAS_KEY){
				$log("MEDIAS KEY found, but key value is not an array : $k->$v",FATAL);
				exit(1);
			}
			$line.=(is_string($replace)) ? "\"$k\"=>$replace," : "\"$k\"=>\"$v\",";
		}
		$line = substr($line,0,-1)."]";
		if(!($dontKeepNoMediasItems && $isNoMediaItem)){
			fwrite($hparsed, "\t$line".(($i<count($data))?",":"")."\n");
			$i++;
		}
	}
	fwrite($hparsed,"];\n");
	fclose($hparsed);
	$log("$compiledFile created.", OK);
	$log("Compilation done (execution time : ".(microtime(true) - START_TIME)."s).",OK);

	if($config["input"]["track_diff"] ?? true){
		$diffTemplate = $config["output"]["track_diff_template"] ?? "found_in_@from_but_not_in_@too.csv";
		$baseFile = array_keys($config["input"]["files"])[0];
		if(count($uniqNotFoundList) > 0){
			foreach($uniqNotFoundList as $name => $list){
				$log(count($list)." items have been found in $name, but not in $baseFile",WARN);
				$notFoundFile = resolvePath(
					str_replace(
							["@from","@too"],
							[$name,$baseFile],
							$diffTemplate
					),
					$config["output"]["root"] ?? null
				);
				$log("Exporting not found products in $notFoundFile...");
				$hnotFound = fopen($notFoundFile,"w");
				$options = $config["input"]["files"][$name];
				$delimiter = $options["delimiter"] ?? $config["input"]["delimiter"] ?? $config["delimiter"] ?? ";";
				fwrite($hnotFound,implode($delimiter,array_keys($options["columns"]))."\n");
				foreach($list as $tmp){
					fwrite($hnotFound,implode($delimiter,$tmp)."\n");
				}
				fclose($hnotFound);
				$log("Missing items report generation done in $notFoundFile",OK);
			}
		}
		$all = $parsedList[$baseFile];
		foreach($parsedList as $name => $list){
			if($name === $baseFile) continue;
			$missing = array_diff(array_keys($all),array_keys($list));
			$log(count($missing)." items have been found in $baseFile, but not in $name",WARN);
			$notFoundFile = resolvePath(
				str_replace(
					["@from","@too"],
					[$baseFile,$name],
					$diffTemplate
				),
				$config["output"]["root"] ?? null
			);
			$log("Exporting not found products in $notFoundFile...");
			$hnotFound = fopen($notFoundFile,"w");
			$options = $config["input"]["files"][$baseFile];
			$delimiter = $options["delimiter"] ?? $config["input"]["delimiter"] ?? $config["delimiter"] ?? ";";
			fwrite($hnotFound,implode($delimiter,array_keys($options["columns"]))."\n");
			$temp = [];
			foreach($missing as $k=>$v){
				$temp[(string)$k]=$all[(string)$v];
			}
			$temp = removeDuplicates(
				array_unique($temp,SORT_REGULAR),
				$indexesOptionsForUniqueCheck
			);
			foreach($temp as $tmp){
				fwrite($hnotFound,implode($delimiter,$tmp)."\n");
			}
			fclose($hnotFound);
			$log("Missing items report generation done in $notFoundFile",OK);
		}
	}else $log("track_diff have been disabled.");

	if(count($noMediasList) > 0 && isset($config["output"]["no_medias"])){
		$log(count($noMediasList)." items haven't been linked to at least one media.",WARN);
		$noMediasFile = $notFoundFile = resolvePath(
			$config["output"]["no_medias"],
			$config["output"]["root"] ?? null
		);
		$log("Exporting items without medias in $noMediasFile...");
		$hnotFound = fopen($notFoundFile,"w");
		$delimiter = $config["output"]["delimiter"] ?? $config["delimiter"] ?? ";";
		fwrite($hnotFound,implode($delimiter,array_diff(array_keys($noMediasList[0]),[MEDIAS_KEY]))."\n");
		foreach($noMediasList as $item){
			unset($item[MEDIAS_KEY]);
			fwrite($hnotFound,implode($delimiter,$item)."\n");
		}
		fclose($hnotFound);
		$log("Missing medias report generation done in $noMediasFile.",OK);
	}
}

if($compileOnly) {
	$log("Compile only mode enabled, process exited.");
	exit(0);
}

if(file_exists($compiledFile)){
	$dataset = require $compiledFile;
	if(empty($dataset)){
		$log("Empty dataset given.",FATAL);
		exit(1);
	}else $log("$compiledFile loaded.",OK);
	$log(count($dataset)." items to process.");
	$header = array_keys($dataset[0]);
	if(!is_dir($storeMediasFolder)){
		mkdir($storeMediasFolder,0777,true);
		$log("Directory $storeMediasFolder created.");
	}
	$addMediasFolderBeforeMediasPath = $config["output"]["include_medias_path"] ?? false;
	$absoluteMediasPaths = $config["output"]["make_medias_paths_absolute"] ?? false;
	$delimiter = $config["output"]["delimiter"] ?? $config["delimiter"] ?? ";";
	$columnsMap = $config["output"]["columns_map"] ?? [];
	$insideDelimiter = $config["output"]["inside_delimiter"] ?? ",";
	$includeNotMappedEntries = $config["output"]["include_not_mapped_entries"] ?? false;
	$stringConvert = $config["output"]["string_converter"] ?? function($value) use($insideDelimiter){
		if(is_array($value)) return implode($insideDelimiter,$value);
		else return (string) $value;
	};
	if(empty($columnsMap)){
		$log("Final columns header : ".implode($delimiter,$header));
		foreach($header as $k){ $columnsMap[$k] = []; }
	} else{
		$filteredColumns = array_filter($columnsMap,function($v){
			if(!is_array($v)) return true;
			return (isset($v["exclude"]) && !$v["exclude"]) || !isset($v["exclude"]);
		});
		$newHeader = array_map(
			function($k,$v){return is_array($v) ? $v["name"] ?? $k : $k;},
			array_keys($filteredColumns),
			$filteredColumns
		);
		$log("Colmuns mapper will process to the following header transformation : "
		     .implode($delimiter,$header)." -> ".implode($delimiter,$newHeader)
		);
		$header = $newHeader;
	}

	$resFile = resolvePath(
		$config["output"]["generated_filename"] ?? "res.csv",
		$config["output"]["root"] ?? $dir
	);
	$lineReplacer = $config["output"]["line_replacer"] ?? function(array $line){ return [$line]; };

	$handle = fopen($resFile,"w");
	fwrite($handle,implode($delimiter,$header)."\n");
	$mediaKey = $columnsMap[MEDIAS_KEY]["name"] ?? MEDIAS_KEY;
	$writtenLine = 1;
	$datasetModifier = $config["output"]["dataset_modifier"] ?? null;
	$mediaFileRenamer = $config["output"]["medias_file_renamer"] ?? function(string $name){return $name;};
	if(is_callable($datasetModifier)) $dataset = $datasetModifier($dataset);
	$totalLines = count($dataset);
	foreach($dataset as $lineo=>$dataLine){
		$log("Processing line $lineo/$totalLines...");
		foreach($dataLine as $key => $value){
			if($key === $mediaKey) foreach($value as $pathIndex => $path){
				if(file_exists($path)){
					$relPath = str_replace("$medias/","",$path);
					$relPath = preg_replace('/[^a-z0-9_\/.-]+/i', "_",$relPath);
					$matches = [];
					preg_match('#\.(.*)$#',$relPath,$matches);
					if(count($matches) > 1)
						$relPath = preg_replace('#\.(.*)$#','.'.strtolower($matches[1]),$relPath);
					if(!is_dir(dirname("$storeMediasFolder/$relPath")))
						mkdir(dirname("$storeMediasFolder/$relPath"),0777,true);
					$relPath = $mediaFileRenamer($relPath);
					if(!file_exists("$storeMediasFolder/$relPath")){
						copy($path,"$storeMediasFolder/$relPath");
						$log("$path copied into $storeMediasFolder/$relPath", OK);
					}
					$dataLine[$key][$pathIndex] = ($addMediasFolderBeforeMediasPath)
						? resolvePath($relPath,$mediasFolder)
						: $relPath;
					if($absoluteMediasPaths && strpos($v[$key][$pathIndex],"/")!==0)
						$dataLine[$key][$pathIndex] = "/".$v[$key][$pathIndex];
				}else{
					$log("Linked media $path not found !",ERR);
				}
			}
		}
		$mappedLine = [];
		$parsedKeys = [];
		$excludedKeys = [];
		foreach($columnsMap as $k=>$opts){
			if(is_callable($opts)) $mappedLine[$k] = $opts($dataLine);
			else if(is_scalar($opts)) $mappedLine[$k] = (string) $opts;
			else if(/*!*/isset($dataLine[$k])){
				/*$log("Please check your \$config['output']['columns_map']['$key']"
				     ." configuration ! It MUST be a callable or a scalar if it defines a new column !",FATAL);
				exit(1);
			}else{*/
				$formater = $opts["formater"] ?? null;
				if(isset($opts["exclude"]) && $opts["exclude"]) $excludedKeys[(string)$k] = $k;
				if(is_callable($formater)) $mappedLine[$opts["name"] ?? $k] = $formater($dataLine[$k]);
				else $mappedLine[$opts["name"] ?? $k] = $dataLine[$k];
			}else continue;
			$parsedKeys[] = $k;
		}
		if($includeNotMappedEntries){
			foreach(array_diff(array_keys($dataLine),$parsedKeys) as $k){
				$mappedLine[$k] = $dataLine[$k];
			}
		}
		$linesToPrint = $lineReplacer($mappedLine, $dataLine);
		foreach($linesToPrint as $k=>$v){
			fwrite(
				$handle,
				mb_convert_encoding(implode(
					$delimiter,
					array_filter($v,function($k)use(&$excludedKeys){
						return !isset($excludedKeys[(string)$k]);
					},ARRAY_FILTER_USE_KEY)
				),'UTF-8','UTF-8')."\n"
			);
			$writtenLine++;
		}
	}
	fclose($handle);
	$log("Final file $resFile created (written line, including header : $writtenLine)",OK);
} else {
	$log(
		"$compiledFile not found, it may have been removed since last execution. "
		."Please retry with -clean argument to generate a new one.",FATAL
	);
	exit(1);
}

$log("Done (total execution time : ".(microtime(true) - START_TIME)."s)",OK);
}catch(Error | Exception $e){
	$log($e,FATAL);
	exit(1);
}